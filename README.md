# Polygon Editor #
### Introduction ###
Everything in this application is based on polygons, which can be created, modified (adding/removing/moving around points) and removed.  
It's possible to take common part of two polygons, which can result in creating either no polygon, 1 polygon or many polygons depending on the situation.

### Textures###
It's also possible for polygons to display loaded textures and use height map and normal vector map in order to create some 3D effects. Every polygon has it's own texture, height map and vector map.  
Every provided texture loaded has it's left top point in left top point of the workspace of application and is set to be repeated (so in case a texture is the same height as workspace and has half of width of workspace, polygon of workspace's size displays loaded texture two times, next to each other)

### Light source ###
All polygons share the same light source which is a 3D point which can be moved around. Light source's color can be changed (it's white by default).