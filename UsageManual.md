# How to use #
There are 2 possible modes: interaction mode (cursor icon) and creation mode (polygon icon).

### Interaction mode: ###
- using mouse you can select polygon's edge or point  
- double click on edge to create new point 

##### Keys: #####
- **a** - changes select mode between selecting single edge/point and selecting whole polygon  
- **m** - move point/polygon  
- **d** - remove point/polygon  
- **c** - clip (get common part of) selected polygon with the next polygon clicked near one of it's points  
- **+/-** - light source distance from texture decrease/increase

Fill with texture and other buttons (except light color which is common for all polygons) require selected polygon (or it's point/edge) in order to work


### Creation mode: ###
- clicking creates points of polygon  
- to create a polygon, you have to click on the first point again  
- polygon must have at least 3 points
