﻿namespace _GRAFIKA_EdytorWielokatow
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanels = new System.Windows.Forms.SplitContainer();
            this.LightColorButton = new System.Windows.Forms.Button();
            this.NormalVectorButton = new System.Windows.Forms.Button();
            this.HeightMapButton = new System.Windows.Forms.Button();
            this.FillWithTexture = new System.Windows.Forms.Button();
            this.Instruction_D = new System.Windows.Forms.Label();
            this.Instruction_M = new System.Windows.Forms.Label();
            this.Instruction_A = new System.Windows.Forms.Label();
            this.ToolPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ToolSelection = new System.Windows.Forms.Button();
            this.ToolCreation = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.Instruction_C = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.MainPanels)).BeginInit();
            this.MainPanels.Panel1.SuspendLayout();
            this.MainPanels.Panel2.SuspendLayout();
            this.MainPanels.SuspendLayout();
            this.ToolPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // MainPanels
            // 
            this.MainPanels.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainPanels.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.MainPanels.IsSplitterFixed = true;
            this.MainPanels.Location = new System.Drawing.Point(0, -2);
            this.MainPanels.Name = "MainPanels";
            // 
            // MainPanels.Panel1
            // 
            this.MainPanels.Panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.MainPanels.Panel1.Controls.Add(this.Instruction_C);
            this.MainPanels.Panel1.Controls.Add(this.LightColorButton);
            this.MainPanels.Panel1.Controls.Add(this.NormalVectorButton);
            this.MainPanels.Panel1.Controls.Add(this.HeightMapButton);
            this.MainPanels.Panel1.Controls.Add(this.FillWithTexture);
            this.MainPanels.Panel1.Controls.Add(this.Instruction_D);
            this.MainPanels.Panel1.Controls.Add(this.Instruction_M);
            this.MainPanels.Panel1.Controls.Add(this.Instruction_A);
            this.MainPanels.Panel1.Controls.Add(this.ToolPanel);
            // 
            // MainPanels.Panel2
            // 
            this.MainPanels.Panel2.Controls.Add(this.pictureBox);
            this.MainPanels.Size = new System.Drawing.Size(1264, 684);
            this.MainPanels.SplitterDistance = 93;
            this.MainPanels.SplitterWidth = 1;
            this.MainPanels.TabIndex = 1;
            // 
            // LightColorButton
            // 
            this.LightColorButton.Location = new System.Drawing.Point(3, 315);
            this.LightColorButton.Name = "LightColorButton";
            this.LightColorButton.Size = new System.Drawing.Size(84, 39);
            this.LightColorButton.TabIndex = 13;
            this.LightColorButton.Text = "Light Color";
            this.LightColorButton.UseVisualStyleBackColor = true;
            this.LightColorButton.Click += new System.EventHandler(this.LightColorButton_Click);
            // 
            // NormalVectorButton
            // 
            this.NormalVectorButton.Location = new System.Drawing.Point(3, 270);
            this.NormalVectorButton.Name = "NormalVectorButton";
            this.NormalVectorButton.Size = new System.Drawing.Size(84, 38);
            this.NormalVectorButton.TabIndex = 12;
            this.NormalVectorButton.Text = "Normal Vector texture";
            this.NormalVectorButton.UseVisualStyleBackColor = true;
            this.NormalVectorButton.Click += new System.EventHandler(this.NormalVectorButton_Click);
            // 
            // HeightMapButton
            // 
            this.HeightMapButton.Location = new System.Drawing.Point(3, 224);
            this.HeightMapButton.Name = "HeightMapButton";
            this.HeightMapButton.Size = new System.Drawing.Size(84, 39);
            this.HeightMapButton.TabIndex = 11;
            this.HeightMapButton.Text = "Height Map";
            this.HeightMapButton.UseMnemonic = false;
            this.HeightMapButton.UseVisualStyleBackColor = true;
            this.HeightMapButton.Click += new System.EventHandler(this.HeightMapButton_Click);
            // 
            // FillWithTexture
            // 
            this.FillWithTexture.Location = new System.Drawing.Point(3, 179);
            this.FillWithTexture.Name = "FillWithTexture";
            this.FillWithTexture.Size = new System.Drawing.Size(84, 39);
            this.FillWithTexture.TabIndex = 10;
            this.FillWithTexture.Text = "Fill with texture";
            this.FillWithTexture.UseVisualStyleBackColor = true;
            this.FillWithTexture.Click += new System.EventHandler(this.FillTextureButton_Click);
            // 
            // Instruction_D
            // 
            this.Instruction_D.AutoSize = true;
            this.Instruction_D.Location = new System.Drawing.Point(12, 135);
            this.Instruction_D.Name = "Instruction_D";
            this.Instruction_D.Size = new System.Drawing.Size(51, 13);
            this.Instruction_D.TabIndex = 3;
            this.Instruction_D.Text = "d - delete";
            // 
            // Instruction_M
            // 
            this.Instruction_M.AutoSize = true;
            this.Instruction_M.Location = new System.Drawing.Point(12, 122);
            this.Instruction_M.Name = "Instruction_M";
            this.Instruction_M.Size = new System.Drawing.Size(50, 13);
            this.Instruction_M.TabIndex = 2;
            this.Instruction_M.Text = "m - move";
            // 
            // Instruction_A
            // 
            this.Instruction_A.AutoSize = true;
            this.Instruction_A.Location = new System.Drawing.Point(12, 109);
            this.Instruction_A.Name = "Instruction_A";
            this.Instruction_A.Size = new System.Drawing.Size(63, 13);
            this.Instruction_A.TabIndex = 1;
            this.Instruction_A.Text = "a - all select";
            // 
            // ToolPanel
            // 
            this.ToolPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ToolPanel.ColumnCount = 1;
            this.ToolPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ToolPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ToolPanel.Controls.Add(this.ToolSelection, 0, 0);
            this.ToolPanel.Controls.Add(this.ToolCreation, 0, 1);
            this.ToolPanel.Location = new System.Drawing.Point(0, 0);
            this.ToolPanel.Name = "ToolPanel";
            this.ToolPanel.RowCount = 2;
            this.ToolPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ToolPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ToolPanel.Size = new System.Drawing.Size(90, 106);
            this.ToolPanel.TabIndex = 0;
            // 
            // ToolSelection
            // 
            this.ToolSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ToolSelection.BackgroundImage = global::_GRAFIKA_EdytorWielokatow.Properties.Resources.SelectionToolBigger;
            this.ToolSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ToolSelection.Location = new System.Drawing.Point(3, 3);
            this.ToolSelection.Name = "ToolSelection";
            this.ToolSelection.Size = new System.Drawing.Size(84, 47);
            this.ToolSelection.TabIndex = 0;
            this.ToolSelection.UseVisualStyleBackColor = true;
            this.ToolSelection.Click += new System.EventHandler(this.ToolSelection_Click);
            // 
            // ToolCreation
            // 
            this.ToolCreation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ToolCreation.BackgroundImage = global::_GRAFIKA_EdytorWielokatow.Properties.Resources.Polygon;
            this.ToolCreation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ToolCreation.Location = new System.Drawing.Point(3, 56);
            this.ToolCreation.Name = "ToolCreation";
            this.ToolCreation.Size = new System.Drawing.Size(84, 47);
            this.ToolCreation.TabIndex = 1;
            this.ToolCreation.UseVisualStyleBackColor = true;
            this.ToolCreation.Click += new System.EventHandler(this.ToolCreation_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1186, 684);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.CanvasClick);
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.CanvasPaint);
            this.pictureBox.DoubleClick += new System.EventHandler(this.Canvas_DoubleClick);
            // 
            // Instruction_C
            // 
            this.Instruction_C.AutoSize = true;
            this.Instruction_C.Location = new System.Drawing.Point(12, 148);
            this.Instruction_C.Name = "Instruction_C";
            this.Instruction_C.Size = new System.Drawing.Size(38, 13);
            this.Instruction_C.TabIndex = 14;
            this.Instruction_C.Text = "c - clip";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.Controls.Add(this.MainPanels);
            this.Name = "MainWindow";
            this.Text = "Polygon Editor";
            this.MainPanels.Panel1.ResumeLayout(false);
            this.MainPanels.Panel1.PerformLayout();
            this.MainPanels.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainPanels)).EndInit();
            this.MainPanels.ResumeLayout(false);
            this.ToolPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.SplitContainer MainPanels;
        private System.Windows.Forms.TableLayoutPanel ToolPanel;
        private System.Windows.Forms.Button ToolSelection;
        private System.Windows.Forms.Button ToolCreation;
        private System.Windows.Forms.Label Instruction_D;
        private System.Windows.Forms.Label Instruction_M;
        private System.Windows.Forms.Label Instruction_A;
        private System.Windows.Forms.Button FillWithTexture;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button HeightMapButton;
        private System.Windows.Forms.Button NormalVectorButton;
        private System.Windows.Forms.Button LightColorButton;
        private System.Windows.Forms.Label Instruction_C;
    }
}

